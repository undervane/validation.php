<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="UTF-8">
		<title>Form</title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700|Raleway:400,500,600,700|Roboto" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/main.css" rel="stylesheet"/>
	
	</head>
	
	<body>
	
		<div class="form-container">
			<form id="accountForm" novalidate>

				<div class="input" id="form-name">
					<label for="img">Image</label>
					<input type="file" name="img"/>
				</div>

				<div class="input" id="form-name">
					<label for="name">Name</label>
					<input type="text" name="name"/>
				</div>

				<div class="input" id="form-email">
					<label for="email">Email</label>
					<input type="email" name="email"/>
				</div>

				<div class="input" id="form-password">
					<label for="password">Password</label>
					<input type="password" name="password"/>
				</div>

				<div class="actions">

					<button id="action-btn" state="register">Register</button>

					<div class="additional">

						<a id="left-option">Forgot my Password</a>
						<span>|</span>
						<a id="right-option">Login to Account</a>

					</div>

				</div>

			</form>
		</div>
	
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="js/error.js"></script>
		<script src="js/form.js"></script>
		<script src="js/main.js"></script>
		
	</body>
	
</html>