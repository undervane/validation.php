# PHP Validation

## Table of Contents

1. [Installation](#1)
2. [Configuration](#2)
3. [Example Use](#3)
4. [FAQ](#4)

### 1. Installation

Put FormValidation.php inside your controllers folder, and *Form.php* and *FormError.php* inside your models folder.

### 2. Configuration

Inside your own controller class (e.g. *UserController.php*), create an import line:

```php
require_once 'controller/FormValidation.php';
``` 
Inside the class itself, create a protected attribute called *$fields*, it will be an array containing all fields:
```php
protected $fields;
```

Now inside the *constructor*, you instantiate the *FormValidation* class like this:

```php
$form = new FormValidation(['name', 'email', 'password']);
```
Here we pass the form structure as an *array parameter*. Here you can change the names to the ones that corresponds to your form.

>**NOTE:** Order and names of the structure is very important. It must correspond to your HTML form inputs' order and names.

Now let's start adding the values and its rules:

```php
$fields[] = ['Pepe', [['required'],['max-length', '10']]];
```

Each field has to correspond to this structure:

```php
[String $value, [[String $rule, String? $optional_detail]];
```
Another example for the email field:

```php
$fields[] = ['test@email.com',[['required'],['email'],['max-length', '10']]];
```

And last for the password:

```php
$fields[] = ['12345',[['required'],['min-length', '3']]];
```

Click [here](#rules) to view a cheatsheet of all **available rules**.

>**NOTE:** The rules' priority is strict, if the former one is not satisfied, the other ones affecting that field will be ommited.
(e.g, if email is *'required'*, and has a *'max-length'* of 10, if the email input is empty, max-length validation will be ommited.)

Now that we've finished adding all fields and rules, we add the array of fields with the *add()* function:

```php
$form->add($fields);
```

And start validation of the fields:

```php
try{

    $form->validate();

    // Logic preceding the function will be executed if the form is valid

} catch (Exception $errors) {

    // Otherwise we catch the exception, and throw an error.

    throw $errors;

}
```
The *Exception* will contain the rules that are not satisfied, and the field affecting that rule. **That's it!**

## 3. Example Use

You can clone the example project on BitBucket from this source: 
```bash
git clone https://undervane@bitbucket.org/undervane/php-validation.git
```

Start your virtual server with **MAMP** / **XAMPP** or your favorite one, and open *index.php* from localhost.

The best way to see how this library actually works is to set a **breakpoint** at this line:

```php
$form = new FormValidation(['name', 'email', 'password']);
```

By using **XDebug**, you can analize it step by step.

## 4. FAQ

### What **rules** do I have **available** to verify my form?

Here you have a cheatsheet of all available rules at this moment:

Rules|Detail|Explanation
---|---|---
*required*|Null|Checks whether the field is **empty** or **filled**
*min-length*|Int|Checks if the field has the indicated **minimum** length
*max-length*|Int|Checks if the field exceeds the indicated **maximum** length
*email*|Null|Checks if the field has the correct email format
*type*|[String]|Checks if the file's mime type is as allowed, refer here to check what types are implemented, and how to add your own 
*max-size*|Int|Checks if file's size exceeds allowed in KB

### Can I add more rules?

Yes, you can modify *FormValidation.php*, inside the *isValid()* function there's a *switch* that evalutes different conditions.

You just have to add a new case indicating the new ***error key***, then inside of it you evaluate the conditions needed to be satisfied, if everything goes well, just add a *break* at the end of the *case statement*, if not you have to throw an ***Exception*** indicating in its *contructor* an *String* **error key***.

### Is this library enough to validate my form?

This library is not bulletproof, it won't allow you to prevent all types of form validation, however it's a good quickstart in which to base and improve your own form validation system.

### What current mime types are available, and how to add more?

Available types are **jpeg**, **jpg**, **png**. However you can add more types verification, inside *FormValidation* **getMimes()** function, you can add more cases containing mime type.

```php
private function getMimes($type){

        switch ($type){

            case 'png':

                return ['image/png', ... 'application/x-png'];

            case 'jpeg' || 'jpg':

                return ['image/jpeg', ... 'image/jpg'];

            // Add here more mime types verification

        }
    }
```

>**NOTE:** filext.com is a great resource to find mime type for files