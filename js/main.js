
/* SETTINGS */

var lang = 'en';

/* DOCUMENT LOAD */

$(function(){

    __initForm();

    $("#accountForm").on('submit', function(e) {

        resetMessages();
        e.preventDefault();

        let formData = createFormData();

        manageRequest(formData);

    });
});

// START - Manage and send an AJAX call

function manageRequest(formData){

    let xhr = new XMLHttpRequest();

    xhr.open("POST", "core/verifyForm.php");

    xhr.responseType = 'json';

    xhr.onreadystatechange = function() {

        if (xhr.readyState == XMLHttpRequest.DONE) {

            let response = xhr.response;
            
            if (response.success == false){

                let errors = JSON.parse(response.errors);

                for (let i = 0; i < errors.length; i++){

                    let error = errors[i];

                    let message = getError(error['error'], lang);
                    let field = error['field'];

                    showError(message, field);

                }

            } else {

                console.log(response);

            }
        }
    }

    xhr.send(formData);

}

// END - Manage and send AJAX call