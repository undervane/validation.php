
// Initialize Form Event Listeners

function __initForm(){

    // PHP Message Error Reset

    $('input').on('focus', function(){

        $(this).siblings('p').remove();

    });

}

// START - Creating FormData Object

function createFormData(){

    let formData = new FormData();

    let img = $('input[name=img')[0].files[0];
    let name = $('input[name=name]').val();
    let email = $('input[name=email]').val();
    let password = $('input[name=password]').val();

    formData.append('img', img);
    formData.append("name", name);
    formData.append("email", email);
    formData.append("password", password);

    return formData;

}

// END - Creating FormData Object

// START - PHP Error Messages

function showError(message, field){

    $(`input[name=${field}]`).after(`<p class="error">${message}</p>`);

}

function resetMessages(){

    $('.error').remove();

}

// END - PHP Error Messages