
// Returns an Error String

function getError(error, lang){

    let messages = `{
        
        "es" : {

        "required" : "Este campo es necesario",
        "min-length" : "Este campo es muy corto",
        "max-length" : "Este campo es muy largo",
        "email" : "El email no es correcto",
        "invalid" : "El tipo de archivo no es correcto",
        "max-size" : "El archivo supera el tamaño permitido"

        }, 

        "en" : {

            "required" : "This field is required",
            "min-length" : "This field is very short",
            "max-length" : "This field is very long",
            "email" : "This email is not correct",
            "invalid" : "File extension is invalid",
            "max-size" : "File size is bigger than allowed"
        
        } 

    }`

    let parsedJSON = JSON.parse(messages);

    return parsedJSON[lang][error];

}