<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/validation.php/model/User.php';
require_once $root.'/validation.php/controller/FormValidation.php';

class UserController extends User{

    protected $fields;

    public function __construct($POST){

        $form = new FormValidation(['img', 'name', 'email', 'password']);

        $fields[] = [$_FILES['img'], [['required'], ['max-size', 2000], ['type', ['png', 'jpeg']]]];
        $fields[] = [$POST['name'], [['required'],['max-length', 10]]];
        $fields[] = [$POST['email'],[['required'],['email']]];
        $fields[] = [$POST['password'],[['required'],['min-length', 3]]];

        $form->add($fields);
        
        try{

            $form->validate();

            $this->_name = $POST['name'];
            $this->_email = $POST['email'];
            $this->_password = $POST['password'];

        } catch (Exception $errors) {

            throw $errors;

        }
    }
}

?>
