<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/validation.php/controller/UserController.php';

try {

    $user = new UserController($_POST);
    echo json_encode(array(

        'success' => true,
        'message' => 'All fine',
        'error' => []
        
    ));

} catch (Exception $errors) {

    $errorsArray = $errors->getMessage();

    echo json_encode(array(

        'success' => false,
        'message' => "There's been some errors on validation",
        'errors' => $errorsArray

    ));

}

?>
